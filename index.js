const search_pokemon = async () => {
    const pokemon_name = document.getElementById("input-search-pokemon").value
    try {
        const response = await fetch(`https://pokeapi.co/api/v2/pokemon/${pokemon_name}`)
        if (response.status < 200 || response.status >= 300) {
            console.log(response.statusText)
            throw Error(`http error received: ${response.status} ${response.statusText}`)
        }
        const data = await response.json()
        document.getElementById("pokemon_front_default").setAttribute("src", data["sprites"]["front_default"])
    } catch (error) {
        document.getElementById("error-log").innerHTML = error.toString()
    }
}